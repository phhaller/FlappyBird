package view;

import javafx.animation.Interpolator;
import javafx.animation.TranslateTransition;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Duration;
import util.ImageManager;

import java.io.IOException;

public class StartWindowController {

  @FXML
  public AnchorPane background;

  private Scene scene;
  private LoginWindow loginWindow;


  public void init(Scene scene) {

    this.scene = scene;
    loginWindow = new LoginWindow(StartWindowController.this);

    createWindow();

  }


  private void createWindow() {

    Rectangle loginRec = new Rectangle(40, 40);
    loginRec.setTranslateX(40);
    loginRec.setTranslateY(30);
    loginRec.setFill(new ImagePattern(ImageManager.miscImgs.get(9)));
    loginRec.getStyleClass().add("loginButton");

    loginRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        TranslateTransition tt = new TranslateTransition();
        tt.setDuration(Duration.millis(600));
        tt.setToX(20);
        tt.setCycleCount(1);
        tt.setAutoReverse(false);
        tt.setInterpolator(Interpolator.EASE_OUT);
        tt.setNode(loginWindow);

        loginWindow.show();
        background.getChildren().add(loginWindow);
        tt.play();
      }
    });

    Rectangle playRec = new Rectangle(116, 68);
    playRec.setTranslateX(1280 / 2 - 116 / 2);
    playRec.setTranslateY(410);
    playRec.setFill(new ImagePattern(ImageManager.miscImgs.get(6)));
    playRec.getStyleClass().add("scoreButton");

    playRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        switchScene();
      }
    });

    Rectangle leaderboardRec = new Rectangle(116, 68);
    leaderboardRec.setTranslateX(1280 / 2 - 116 / 2);
    leaderboardRec.setTranslateY(485);
    leaderboardRec.setFill(new ImagePattern(ImageManager.miscImgs.get(7)));
    leaderboardRec.getStyleClass().add("scoreButton");

    leaderboardRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        background.getChildren().add(new LeaderboardWindow(background));
      }
    });


    background.getChildren().addAll(loginRec, playRec, leaderboardRec);

  }


  private void switchScene() {

    try {

      FXMLLoader loader = loader = new FXMLLoader(getClass().getResource("/fxml/gameWindow.fxml"));
      Parent root = loader.load();
      Stage stage = (Stage) background.getScene().getWindow();

      GameWindowController controller = loader.getController();
      controller.init(scene, loginWindow.getPlayerName());

      stage.getScene().setRoot(root);

    } catch (IOException e) {
      e.printStackTrace();
    }

  }

}
