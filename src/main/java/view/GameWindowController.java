package view;

import components.Bird;
import components.Ground;
import components.Obstacle;
import javafx.animation.Animation;
import javafx.animation.AnimationTimer;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;
import util.DatabaseManager;
import util.ImageManager;

import java.util.ArrayList;
import java.util.Iterator;

public class GameWindowController {

  @FXML
  public AnchorPane background;

  private Scene scene;
  public String playerName;

  private AnchorPane numberPane;

  private Bird bird;
  private ArrayList<Obstacle> obstacles;
  private ArrayList<Ground> grounds;

  private Timeline startTimeline;
  private int startTime;

  private AnimationTimer animationTimer;
  private int spawnObstacleCounter = 0;

  private Rectangle menuRec;

  // private Label scoreLabel;
  private AnchorPane scorePane;
  private int score;
  private int speedIncrease = 0;

  public void init(Scene scene, String playerName) {

    this.scene = scene;
    obstacles = new ArrayList<>();
    grounds = new ArrayList<>();

    if (playerName == null || playerName.isEmpty()) {
      this.playerName = "Guest";
    } else {
      this.playerName = playerName;
    }


    createBird();
    createInitGround();
    createObstacle();
    createScore();

    createHandlers();

    createStartUp();

  }


  private void createBird() {
    bird = new Bird();
    background.getChildren().add(bird);
  }


  private void createInitGround() {
    Ground g1 = new Ground();
    g1.setTranslateX(g1.getTranslateX() - 4 * 400);
    Ground g2 = new Ground();
    g2.setTranslateX(g2.getTranslateX() - 3 * 400);
    Ground g3 = new Ground();
    g3.setTranslateX(g3.getTranslateX() - 2 * 400);
    Ground g4 = new Ground();
    g4.setTranslateX(g4.getTranslateX() - 400);
    Ground g5 = new Ground();

    grounds.add(g1);
    grounds.add(g2);
    grounds.add(g3);
    grounds.add(g4);
    grounds.add(g5);

    background.getChildren().addAll(g1, g2, g3, g4, g5);
  }


  private void createGround() {
    Ground ground = new Ground();
    grounds.add(ground);
    background.getChildren().add(ground);
  }


  private void createObstacle() {
    Obstacle obstacle = new Obstacle(speedIncrease);
    obstacles.add(obstacle);
    background.getChildren().add(obstacle);
  }


  private void createScore() {

    score = 0;

    scorePane = ImageManager.getNumberImage(String.valueOf(score), 50, 75);
    scorePane.setTranslateX(1280 / 2 - scorePane.getPrefWidth() / 2);
    scorePane.setTranslateY(20);

  }


  private void refreshScore() {
    background.getChildren().remove(scorePane);
    background.getChildren().add(scorePane);
  }


  private void createHandlers() {

    scene.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
      @Override
      public void handle(KeyEvent keyEvent) {

        if (keyEvent.getCode() == KeyCode.SPACE) {
          if (!bird.jumping) {
            bird.jump();
          }
        }

      }
    });

  }


  private void createStartUp() {

    Rectangle getReadyRec = new Rectangle(430, 120);
    getReadyRec.setTranslateX(1280 / 2 - 430 / 2);
    getReadyRec.setTranslateY(220);
    getReadyRec.setFill(new ImagePattern(ImageManager.miscImgs.get(0)));

    startTime = 3;
    numberPane = ImageManager.getNumberImage(String.valueOf(startTime), 50, 75);
    numberPane.setTranslateX(1280 / 2 - numberPane.getPrefWidth() / 2);
    numberPane.setTranslateY(360);

    background.getChildren().addAll(getReadyRec, numberPane);

    startTimeline = new Timeline(new KeyFrame(Duration.seconds(1), new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {

        startTime--;
        background.getChildren().remove(numberPane);
        numberPane = ImageManager.getNumberImage(String.valueOf(startTime), 50, 75);
        numberPane.setTranslateX(1280 / 2 - numberPane.getPrefWidth() / 2);
        numberPane.setTranslateY(360);
        background.getChildren().add(numberPane);

        if (startTime <= 0) {
          startTimeline.stop();
          background.getChildren().removeAll(getReadyRec, numberPane);

          background.getChildren().add(scorePane);

          if (animationTimer == null) {
            createGameLoop();
          } else {
            animationTimer.start();
          }

          bird.jump();
        }

      }
    }));

    startTimeline.setCycleCount(Animation.INDEFINITE);
    startTimeline.play();


  }


  private void createGameLoop() {

    animationTimer = new AnimationTimer() {
      @Override
      public void handle(long l) {

        update();

      }
    };

    animationTimer.start();

  }


  private void update() {

    // BIRD
    bird.update();
    boolean collidedWithFloorOrCeiling = bird.checkFloorAndCeiling();
    if (collidedWithFloorOrCeiling) {
      animationTimer.stop();
      // reset();
      background.getChildren().remove(scorePane);

      background.getChildren().add(new ScoreWindow(this, score, 4));
    }


    // OBSTACLES
    for (Iterator<Obstacle> it = obstacles.iterator(); it.hasNext();) {
      Obstacle obstacle = it.next();
      obstacle.update();

      if (obstacle.getTranslateX() + 100 < bird.getTranslateX() && !obstacle.counted) {
        score++;
        background.getChildren().remove(scorePane);
        scorePane = ImageManager.getNumberImage(String.valueOf(score), 50, 75);
        scorePane.setTranslateX(1280 / 2 - scorePane.getPrefWidth() / 2);
        scorePane.setTranslateY(20);
        background.getChildren().add(scorePane);
        obstacle.counted = true;
      }

      boolean collidedWithWorldBorder = obstacle.checkWorldBorder();
      if (collidedWithWorldBorder) {
        it.remove();
        background.getChildren().remove(obstacle);
      }

      boolean checkBirdObstacleCollision = obstacle.checkBirdCollision(bird);
      if (checkBirdObstacleCollision) {
        animationTimer.stop();
        // reset();
        background.getChildren().remove(scorePane);
        background.getChildren().add(new ScoreWindow(this, score, 4));
      }
    }

    if (spawnObstacleCounter > 80) {
      createObstacle();
      refreshScore();
      spawnObstacleCounter = 0;
    }

    spawnObstacleCounter++;


    // GROUND
    boolean needsGround = false;
    for (Iterator<Ground> it = grounds.iterator(); it.hasNext();) {

      Ground ground = it.next();
      ground.update();

      if (ground.checkWorldBorder()) {
        it.remove();
        background.getChildren().remove(ground);
        needsGround = true;
      }

    }

    if (needsGround) {
      createGround();
    }

  }


  public void reset() {
    for (AnchorPane obstacle : obstacles) {
      background.getChildren().remove(obstacle);
    }
    obstacles.clear();
    spawnObstacleCounter = 0;
    score = 0;
    background.getChildren().remove(scorePane);
    scorePane = ImageManager.getNumberImage(String.valueOf(score), 50, 75);
    scorePane.setTranslateX(1280 / 2 - scorePane.getPrefWidth() / 2);
    scorePane.setTranslateY(20);

    createObstacle();
    // refreshScoreAndMenu();
    bird.reset();

    createStartUp();
  }

}
