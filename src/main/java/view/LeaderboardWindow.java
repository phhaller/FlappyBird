package view;

import javafx.event.EventHandler;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import util.DatabaseManager;
import util.ImageManager;
import util.LeaderboardEntry;

import java.sql.SQLException;
import java.util.ArrayList;

public class LeaderboardWindow extends AnchorPane {

  private AnchorPane background;
  private TableView tableView;
  private ArrayList<String> leaderboardEntries;


  public LeaderboardWindow(AnchorPane background) {

    this.background = background;

    createWindow();
    createTableView();
    fillLeaderboard();

  }


  private void createWindow() {

    setPrefSize(900, 600);
    setTranslateX(190);
    setTranslateY(50);
    getStyleClass().add("leaderboard");

    Rectangle closeRec = new Rectangle(20, 20);
    closeRec.setTranslateX(845);
    closeRec.setTranslateY(30);
    closeRec.getStyleClass().add("close");
    closeRec.setFill(new ImagePattern(ImageManager.miscImgs.get(10)));

    closeRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        background.getChildren().remove(LeaderboardWindow.this);
      }
    });

    getChildren().addAll(closeRec);

  }


  private void createTableView() {

    tableView = new TableView();
    tableView.setEditable(false);
    tableView.setPrefSize(800, 500);
    tableView.setTranslateX(50);
    tableView.setTranslateY(50);

    TableColumn<String, LeaderboardEntry> column1 = new TableColumn<>("Place");
    column1.setCellValueFactory(new PropertyValueFactory<>("place"));
    column1.setPrefWidth(100);
    column1.getStyleClass().add("leaderboardTableViewColumn");
    column1.setReorderable(false);
    column1.setSortable(false);

    TableColumn<String, LeaderboardEntry> column2 = new TableColumn<>("Username");
    column2.setCellValueFactory(new PropertyValueFactory<>("userName"));
    column2.setPrefWidth(400);
    column2.getStyleClass().add("candidateTableColumn");
    column2.setReorderable(false);
    column2.setSortable(false);

    TableColumn<String, LeaderboardEntry> column3 = new TableColumn<>("Score");
    column3.setCellValueFactory(new PropertyValueFactory<>("score"));
    column3.setPrefWidth(280);
    column3.getStyleClass().add("candidateTableColumn");
    column3.setSortType(TableColumn.SortType.DESCENDING);
    column3.setReorderable(false);
    column3.setSortable(false);

    tableView.getColumns().addAll(column1, column2, column3);
    tableView.getStyleClass().add("leaderboardTableView");
    tableView.setFocusTraversable(false);
    tableView.setSelectionModel(null);


    getChildren().addAll(tableView);

  }


  public void fillLeaderboard() {

    try {
      leaderboardEntries = DatabaseManager.readAllEntries();

      for(int i = 0; i < leaderboardEntries.size(); i++) {

        String[] tmp = leaderboardEntries.get(i).split("§");
        tableView.getItems().add(new LeaderboardEntry(i + 1, tmp[0], Integer.parseInt(tmp[1])));

      }
    } catch (SQLException e) {
      e.printStackTrace();
    }

  }

}
