package view;

import components.Medal;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import util.DatabaseManager;
import util.ImageManager;

import java.io.IOException;

public class ScoreWindow extends AnchorPane {

  private GameWindowController gwc;

  private int score;
  private int bestScore;
  private int windowWidth = 702;
  private int windowHeight = 360;

  private AnchorPane scorePane;
  private AnchorPane bestScorePane;
  private Medal medal;


  public ScoreWindow(GameWindowController gwc, int score, int bestScore) {

    this.gwc = gwc;
    this.score = score;
    this.bestScore = bestScore;

    createWindow();

  }


  private void createWindow() {

    setPrefSize(windowWidth, windowHeight);
    setTranslateX(1280 / 2 - windowWidth / 2);
    setTranslateY(800 / 2 - windowHeight / 2 - 50);
    setStyle("-fx-background-image: url(/images/scoreWindow.png)");

    scorePane = ImageManager.getNumberImage(String.valueOf(score), 30, 45);
    scorePane.setTranslateX(620 - scorePane.getPrefWidth());
    scorePane.setTranslateY(110);

    // Save score to database
    if (DatabaseManager.containsPlayer(gwc.playerName)) {
      if (DatabaseManager.getScoreByPlayer(gwc.playerName) < score) {
        DatabaseManager.updateEntry(gwc.playerName, score);
      }
    } else {
      DatabaseManager.createEntry(gwc.playerName, score);
    }

    bestScorePane = ImageManager.getNumberImage(String.valueOf(DatabaseManager.getMaxScore()), 30, 45);
    bestScorePane.setTranslateX(620 - bestScorePane.getPrefWidth());
    bestScorePane.setTranslateY(238);

    Rectangle medal = getMedal();

    Rectangle playRec = new Rectangle(116, 68);
    playRec.setTranslateX(167);
    playRec.setTranslateY(800 / 2 - windowHeight / 2 + 140);
    playRec.setFill(new ImagePattern(ImageManager.miscImgs.get(6)));
    playRec.getStyleClass().add("scoreButton");

    playRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        gwc.background.getChildren().remove(ScoreWindow.this);
        gwc.reset();
      }
    });

    Rectangle leaderboardRec = new Rectangle(116, 68);
    leaderboardRec.setTranslateX(293);
    leaderboardRec.setTranslateY(800 / 2 - windowHeight / 2 + 140);
    leaderboardRec.setFill(new ImagePattern(ImageManager.miscImgs.get(7)));
    leaderboardRec.getStyleClass().add("scoreButton");

    leaderboardRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        gwc.background.getChildren().add(new LeaderboardWindow(gwc.background));
      }
    });

    Rectangle backRec = new Rectangle(116, 68);
    backRec.setTranslateX(419);
    backRec.setTranslateY(800 / 2 - windowHeight / 2 + 140);
    backRec.setFill(new ImagePattern(ImageManager.miscImgs.get(8)));
    backRec.getStyleClass().add("scoreButton");

    backRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        switchScene();
      }
    });

    getChildren().addAll(scorePane, bestScorePane, medal, playRec, leaderboardRec, backRec);

  }


  private Rectangle getMedal() {
    Medal medal = new Medal(score);
    medal.setTranslateX(90);
    medal.setTranslateY(138);
    return medal;
  }


  private void switchScene() {

    try {

      FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/startWindow.fxml"));
      Parent root = loader.load();
      Stage stage = (Stage) gwc.background.getScene().getWindow();

      StartWindowController controller = loader.getController();
      controller.init(gwc.background.getScene());

      stage.getScene().setRoot(root);

    } catch (IOException e) {
      e.printStackTrace();
    }

  }

}
