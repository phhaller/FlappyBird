package view;

import javafx.animation.Interpolator;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;
import util.ImageManager;

public class LoginWindow extends AnchorPane {

  private StartWindowController swc;

  private int width;
  private int height;

  private TranslateTransition translateTransition;
  private Rectangle closeRec;
  private TextField playerNameTextfield;

  private String playerName;

  private boolean loggedIn;


  public LoginWindow(StartWindowController swc) {

    this.swc = swc;

    width = 351;
    height = 180;
    playerName = "";
    loggedIn = false;

    createWindow();
    createHandler();
    createTransitions();

  }


  private void createWindow() {

    setPrefSize(width, height);
    setTranslateX(-width);
    setTranslateY(15);
    getStyleClass().add("loginWindow");

    closeRec = new Rectangle(20, 20);
    closeRec.setTranslateX(310);
    closeRec.setTranslateY(20);
    closeRec.getStyleClass().add("close");
    closeRec.setFill(new ImagePattern(ImageManager.miscImgs.get(10)));

    playerNameTextfield = new TextField();
    playerNameTextfield.setPrefSize(251, 30);
    playerNameTextfield.setTranslateX(52);
    playerNameTextfield.setTranslateY(60);
    playerNameTextfield.setPromptText("Enter username");
    playerNameTextfield.getStyleClass().add("playerNameTextfield");
    playerNameTextfield.setFocusTraversable(false);

    Rectangle saveRec = new Rectangle(72, 40);
    saveRec.setTranslateX(width / 2 - 72 / 2);
    saveRec.setTranslateY(110);
    saveRec.setFill(new ImagePattern(ImageManager.miscImgs.get(11)));
    saveRec.getStyleClass().add("scoreButton");

    saveRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        if (!playerNameTextfield.getText().isEmpty()) {
          playerName = playerNameTextfield.getText();
          loggedIn = true;

          translateTransition.play();
        }
      }
    });

    getChildren().addAll(closeRec, playerNameTextfield, saveRec);

  }


  private void createHandler() {

    closeRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        translateTransition.play();
      }
    });

  }


  private void createTransitions() {

    translateTransition = new TranslateTransition();
    translateTransition.setDuration(Duration.millis(600));
    translateTransition.setToX(-350);
    translateTransition.setCycleCount(1);
    translateTransition.setAutoReverse(false);
    translateTransition.setInterpolator(Interpolator.LINEAR);
    translateTransition.setNode(LoginWindow.this);

    translateTransition.setOnFinished(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        swc.background.getChildren().remove(LoginWindow.this);
      }
    });

  }


  public void show() {

    if (loggedIn) {

      getChildren().clear();

      Label loggedInLabel = new Label("You're logged in as");
      loggedInLabel.setPrefSize(251, 30);
      loggedInLabel.setTranslateX(52);
      loggedInLabel.setTranslateY(40);
      loggedInLabel.getStyleClass().add("loggedInLabel");

      Label playerLabel = new Label(playerName);
      playerLabel.setPrefSize(251, 30);
      playerLabel.setTranslateX(52);
      playerLabel.setTranslateY(80);
      playerLabel.getStyleClass().add("playerLabel");


      getChildren().addAll(loggedInLabel, playerLabel, closeRec);

    }

  }


  public String getPlayerName() {
    return playerName;
  }

}
