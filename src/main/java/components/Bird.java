package components;

import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Rotate;
import util.ImageManager;

public class Bird extends Rectangle {

  public boolean jumping = false;
  private final double jumpStrength = 13.0;
  private final double weight = .6;
  private double curJumpStrength  = 0.0;
  private double curWeight = 0.0;

  private Rotate rotate;

  public Bird() {

    setWidth(40);
    setHeight(30);
    setTranslateX(238);
    setTranslateY(398);
    setFill(new ImagePattern(ImageManager.birdImgs.get(0)));

    rotate = new Rotate();
    rotate.setAngle(0);
    rotate.setPivotX(20);
    rotate.setPivotY(15);

    getTransforms().add(rotate);

  }


  public void jump() {
    jumping = true;
    curJumpStrength = jumpStrength;
    curWeight = weight;
  }


  public void reset() {
    jumping = false;
    curJumpStrength = 0.0;
    curWeight = 0.0;
    setTranslateX(238);
    setTranslateY(398);
  }


  public void update() {

    setTranslateY(getTranslateY() - curJumpStrength);
    curJumpStrength -= curWeight;

    if (curJumpStrength < 0) {
      jumping = false;
    }

    if (curJumpStrength < -15) {
      curJumpStrength = -15;
    }

    rotate.setAngle(-curJumpStrength);

  }


  public boolean checkFloorAndCeiling() {
    boolean collided = false;
    if (getTranslateY() < 0 || getTranslateY() >= 670) {
      collided = true;
    }
    return collided;
  }

}
