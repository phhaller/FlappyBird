package components;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import util.ImageManager;

import java.util.Random;

public class Obstacle extends AnchorPane {

  private Rectangle bottomRec;
  private Rectangle topRec;
  public int scrollSpeed = 8;
  public boolean counted = false;

  public Obstacle(int speedIncrease) {

    setPrefSize(130, 800);
    setTranslateX(1300);
    setTranslateY(0);

    scrollSpeed += speedIncrease;

    createRecs();
  }


  private void createRecs() {

    Random r = new Random();
    int bottomHeight = r.nextInt(400 - 100) + 100;
    int topHeight = 700 - bottomHeight - 200;

    Image bottom = ImageManager.obstacleImgs.get(0);
    PixelReader bottomReader = bottom.getPixelReader();

    bottomRec = new Rectangle();
    bottomRec.setWidth(130);
    bottomRec.setHeight(bottomHeight);
    bottomRec.setTranslateX(0);
    bottomRec.setTranslateY(800 - bottomHeight - 100);
    // bottomRec.getStyleClass().add("obstacle");

    WritableImage bottomImg = new WritableImage(bottomReader, 0, 0, 130, bottomHeight);
    bottomRec.setFill(new ImagePattern(bottomImg));


    Image top = ImageManager.obstacleImgs.get(1);
    PixelReader topReader = top.getPixelReader();

    topRec = new Rectangle();
    topRec.setWidth(130);
    topRec.setHeight(topHeight);
    topRec.setTranslateX(0);
    topRec.setTranslateY(0);
    // topRec.getStyleClass().add("obstacle");

    WritableImage topImg = new WritableImage(topReader, 0, 500 - topHeight, 130, topHeight);
    topRec.setFill(new ImagePattern(topImg));

    getChildren().addAll(bottomRec, topRec);

  }


  public void update() {
    setTranslateX(getTranslateX() - scrollSpeed);
  }


  public boolean checkWorldBorder() {
    boolean collided = false;
    if (getTranslateX() < -150) {
      collided = true;
    }
    return collided;
  }


  public boolean checkBirdCollision(Rectangle bird) {
    boolean collided = false;

    Shape bottomIntersect = Shape.intersect(bird, bottomRec);
    Shape topIntersect = Shape.intersect(bird, topRec);

    if (bottomIntersect.getBoundsInParent().getWidth() > 0 ||
        topIntersect.getBoundsInParent().getWidth() > 0) {
      collided = true;
    }

    return collided;
  }

}
