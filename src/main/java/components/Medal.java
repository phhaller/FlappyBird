package components;

import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import util.ImageManager;

public class Medal extends Rectangle {

  private int score;


  public Medal(int score) {

    this.score = score;
    createMedal();

  }


  private void createMedal() {

    setWidth(132);
    setHeight(132);

    if (score >= 50) {
      setFill(new ImagePattern(ImageManager.miscImgs.get(5)));
    } else if (score >= 30) {
      setFill(new ImagePattern(ImageManager.miscImgs.get(4)));
    } else if (score >= 20) {
      setFill(new ImagePattern(ImageManager.miscImgs.get(3)));
    } else if (score >= 10) {
      setFill(new ImagePattern(ImageManager.miscImgs.get(2)));
    } else {
      setFill(Color.TRANSPARENT);
    }

  }

}
