package components;

import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import util.ImageManager;

public class Ground extends Rectangle {

  public int scrollSpeed = 8;
  public boolean counted = false;


  public Ground() {

    setWidth(400);
    setHeight(130);
    setTranslateX(1480);
    setTranslateY(670);
    setFill(new ImagePattern(ImageManager.groundImgs.get(0)));

  }


  public void update() {
    setTranslateX(getTranslateX() - scrollSpeed);
  }


  public boolean checkWorldBorder() {
    boolean collided = false;
    if (getTranslateX() <= -520) {
      collided = true;
    }
    return collided;
  }


}
