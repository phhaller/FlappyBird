package util;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;

import java.util.ArrayList;

public class ImageManager {

  public static ArrayList<Image> birdImgs = new ArrayList<>();
  public static ArrayList<Image> obstacleImgs = new ArrayList<>();
  public static ArrayList<Image> groundImgs = new ArrayList<>();
  public static ArrayList<Image> numberImgs = new ArrayList<>();
  public static ArrayList<Image> miscImgs = new ArrayList<>();


  public ImageManager() {
    loadImgs();
  }


  private void loadImgs() {

    // BIRD
    birdImgs.add(new Image("/images/bird.png"));


    // OBSTACLE
    obstacleImgs.add(new Image("/images/pipeBottom.png"));
    obstacleImgs.add(new Image("/images/pipeTop.png"));


    // GROUND
    groundImgs.add(new Image("/images/groundDirt-1.png"));
    groundImgs.add(new Image("/images/groundSand.png"));


    // NUMBER
    numberImgs.add(new Image("/images/0.png"));
    numberImgs.add(new Image("/images/1.png"));
    numberImgs.add(new Image("/images/2.png"));
    numberImgs.add(new Image("/images/3.png"));
    numberImgs.add(new Image("/images/4.png"));
    numberImgs.add(new Image("/images/5.png"));
    numberImgs.add(new Image("/images/6.png"));
    numberImgs.add(new Image("/images/7.png"));
    numberImgs.add(new Image("/images/8.png"));
    numberImgs.add(new Image("/images/9.png"));


    // MISC
    miscImgs.add(new Image("/images/getReady.png"));
    miscImgs.add(new Image("/images/scoreWindow.png"));
    miscImgs.add(new Image("/images/bronzeMedal.png"));
    miscImgs.add(new Image("/images/silverMedal.png"));
    miscImgs.add(new Image("/images/goldMedal.png"));
    miscImgs.add(new Image("/images/platinumMedal.png"));
    miscImgs.add(new Image("/images/playButton.png"));
    miscImgs.add(new Image("/images/leaderboardButton.png"));
    miscImgs.add(new Image("/images/backButton.png"));
    miscImgs.add(new Image("/images/login.png"));
    miscImgs.add(new Image("/images/close.png"));
    miscImgs.add(new Image("/images/saveButton.png"));

  }


  public static AnchorPane getNumberImage(String numberString, int numberWidth, int numberHeight) {

    int curWidth = 0;
    int gap = 0;
    AnchorPane anchorPane = new AnchorPane();
    anchorPane.setPrefSize(numberWidth * numberString.length() + gap * numberString.length() - 1, numberHeight);

    for (int i = 0; i < numberString.length(); i++) {
      int nr = Integer.parseInt(String.valueOf(numberString.charAt(i)));
      Rectangle rec = new Rectangle(numberWidth, numberHeight);
      rec.setTranslateX(curWidth);
      rec.setTranslateY(0);
      rec.setFill(new ImagePattern(ImageManager.numberImgs.get(nr)));

      anchorPane.getChildren().add(rec);
      curWidth += numberWidth + gap;
    }

    return anchorPane;

  }

}
