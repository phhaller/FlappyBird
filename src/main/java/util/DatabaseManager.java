package util;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;

public class DatabaseManager {

  private String os = System.getProperty("os.name");
  private String userName = System.getProperty("user.name");

  private final String DB_NAME = "FlappyBird_DB.db";
  private String CONNECTION_STRING = "";

  private final static String TABLE_HIGHSCORES = "highscores";
  private final static String COLUMN_PLAYERNAME = "player_name";
  private final static String COLUMN_SCORE = "score";

  private static Connection connection;

  public DatabaseManager() {

    if (os.startsWith("Mac")) {
      String dbPath = "/Users/" + userName + "/Documents/FlappyBird";
      boolean f = new File(dbPath).mkdirs();
      CONNECTION_STRING = "jdbc:sqlite:" + dbPath + "/" + DB_NAME;
    }

    if (os.startsWith("Windows")) {
      String dbPath = "C:\\Users\\" + userName + "\\Documents\\FlappyBird";
      boolean f = new File(dbPath).mkdirs();
      CONNECTION_STRING = "jdbc:sqlite:" + dbPath + "\\" + DB_NAME;
    }


    try {

      connection = DriverManager.getConnection(CONNECTION_STRING);

      connection.createStatement().execute("CREATE TABLE IF NOT EXISTS " +
          TABLE_HIGHSCORES + "( " +
          COLUMN_PLAYERNAME + " TEXT, " +
          COLUMN_SCORE + " INTEGER)");

    } catch (SQLException e) {
      e.printStackTrace();
    }

  }


  public static void createEntry(String playerName, int score) {

    try {

      connection.createStatement().execute("INSERT INTO " +
          TABLE_HIGHSCORES + " VALUES " + "('" +
          playerName + "', '" +
          score + "')");

    } catch (SQLException e) {
      e.printStackTrace();
    }

  }


  public static void updateEntry(String playerName, int score) {

    try {

      connection.createStatement().execute("UPDATE " + TABLE_HIGHSCORES +
          " SET " + COLUMN_SCORE + " = '" + score + "'" +
          " WHERE " + COLUMN_PLAYERNAME + " = '" + playerName + "'" + "COLLATE  NOCASE");

    } catch (SQLException e) {
      e.printStackTrace();
    }

  }


  public static ArrayList readAllEntries() throws SQLException {

    ResultSet resultSet = connection.createStatement().executeQuery("SELECT * FROM " + TABLE_HIGHSCORES + " ORDER BY " + COLUMN_SCORE + " DESC");
    ArrayList<String> dbEntries = new ArrayList<>();

    dbEntries = getResults(resultSet);

    return dbEntries;

  }


  public static int getMaxScore() {

    int max = 0;

    try {
      ResultSet resultSet = connection.createStatement().executeQuery("SELECT  MAX(" + COLUMN_SCORE + ") AS max_score FROM " + TABLE_HIGHSCORES);
      max = resultSet.getInt("max_score");
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return max;

  }


  public static boolean containsPlayer(String playerName) {

    boolean contains = false;

    try {
      ResultSet resultSet = connection.createStatement().executeQuery("SELECT * FROM " + TABLE_HIGHSCORES +
          " WHERE " + COLUMN_PLAYERNAME + " == '" + playerName + "'" + "COLLATE  NOCASE");
      contains = resultSet.getInt(1) == 0;
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return contains;

  }


  public static int getScoreByPlayer(String playerName) {

    int score = 0;

    try {
      ResultSet resultSet = connection.createStatement().executeQuery("SELECT "+ COLUMN_SCORE +" AS player_score  FROM " + TABLE_HIGHSCORES +
          " WHERE " + COLUMN_PLAYERNAME + " == '" + playerName + "'" + "COLLATE  NOCASE");
      score = resultSet.getInt("player_score");
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return score;

  }


  private static ArrayList<String> getResults(ResultSet resultSet) throws SQLException {

    ArrayList<String> entries = new ArrayList<>();

    while (resultSet.next()) {
      entries.add(resultSet.getString(COLUMN_PLAYERNAME) + "§" +
          resultSet.getInt(COLUMN_SCORE)
      );

    }

    return entries;

  }



}
