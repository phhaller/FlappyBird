package util;


public class LeaderboardEntry {

  private int place;
  private String userName;
  private int score;

  public LeaderboardEntry(int place, String userName, int score) {
    this.place = place;
    this.userName = userName;
    this.score = score;
  }


  public int getPlace() {
    return place;
  }

  public void setPlace(int place) {
    this.place = place;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public int getScore() {
    return score;
  }

  public void setScore(int score) {
    this.score = score;
  }

}