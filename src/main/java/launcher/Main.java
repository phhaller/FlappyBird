package launcher;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import util.DatabaseManager;
import util.ImageManager;
import view.GameWindowController;
import view.StartWindowController;

import java.io.IOException;

public class Main extends Application {

  private Stage primaryStage;

  // Initialize imageManager so that all images are loaded only once
  private ImageManager imageManager = new ImageManager();
  private DatabaseManager databaseManager = new DatabaseManager();

  @Override
  public void start(Stage primaryStage) throws Exception {

    this.primaryStage = primaryStage;
    this.primaryStage.setTitle("Flappy Birds");

    createWindow();

  }


  private void createWindow() {

    try {

      FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/startWindow.fxml"));
      AnchorPane mainWindow = loader.load();

      Scene scene = new Scene(mainWindow);

      StartWindowController controller = loader.getController();
      controller.init(scene);

      primaryStage.setScene(scene);
      primaryStage.setResizable(false);

      primaryStage.show();

    } catch (IOException e) {
      e.printStackTrace();
    }
  }


  public static void main(String[] args) {
    launch(args);
  }


}
